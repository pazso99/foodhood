<?php

namespace App\Models\Database;

use \PDO;
use \Exception;
use \Dotenv\Dotenv;


class Database
{
    protected function connect()
    {
        try {
            $dotenv = Dotenv::createImmutable('./../');
            $dotenv->load();

            $adapter = $_ENV['DB_ADAPTER'];
            $host = $_ENV['DB_HOST'];
            $user = $_ENV['DB_USER'];
            $password = $_ENV['DB_PASSWORD'];
            $dbName = $_ENV['DB_NAME'];

            $dsn = "{$adapter}:host={$host};dbname={$dbName}";
            $pdo = new PDO($dsn, $user, $password);

            # default fetch : associative arrays
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            return $pdo;
        } catch (Exception $e) {
            echo "CONNECTION FAILED: {$e}";
        }
    }
}
