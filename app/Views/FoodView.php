<?php

namespace App\Views;

use App\Controllers\FoodController;

class FoodView extends FoodController
{
    public function showFoods($amount)
    {
        $count = $this->getFoodCount();

        if ($amount <= $count) {
        
            $foodDetails = $this->getFoods($amount);

            $html = "";
            $iterator = 0;
            foreach($foodDetails as $i => $food) {
                
                $foodName = $food['name'];
                $foodDesc = $food['description'];
                $foodPrice = $food['price'];
                $foodType = $food['type'];
                $foodPopularity = $food['popularity'];
                $count = count($foodDetails);

                if ($i == $count - 1) {
                    $html .=
                    "
                    <div class='card border-0 m-2 p-2 d-flex align-items-center text-center'>
                        <img class='card-img-top' style='max-width:308px' src='img/{$foodType}0.jpg' alt='Card image cap'>
                        <div class='card-body'>
                            <h5 class='card-title'>{$foodName}</h5>
                            <p class='card-text'>{$foodDesc}</p>
                            <p class='card-text'>{$foodPrice} Ft - Popularity: {$foodPopularity}</p>
                        </div>
                    </div>
                    </div>
                    ";
                } else {
                    if ($iterator == 0) {
                        $html .= "<div class='card-group m-2 p-2 d-flex align-items-start justify-content-center text-center'>";
                        $iterator++;
                    }
                    if ($iterator <= 3) {
                        $html .=
                        "
                        <div class='card border-0 m-2 p-2 d-flex align-items-center text-center'>
                            <img class='card-img-top' style='max-width:308px' src='img/{$foodType}{$iterator}.jpg' alt='Card image cap'>
                            <div class='card-body'>
                                <h5 class='card-title'>{$foodName}</h5>
                                <p class='card-text'>{$foodDesc}</p>
                                <p class='card-text'>{$foodPrice} Ft - Popularity: {$foodPopularity}</p>
                            </div>
                        </div>
                        ";
                        $iterator++;
                    } else {
                        $iterator = 0;
                        $html .= "
                        <div class='card border-0 m-2 p-2 d-flex align-items-center text-center'>
                            <img class='card-img-top' style='max-width:308px' src='img/{$foodType}{$iterator}.jpg' alt='Card image cap'>
                            <div class='card-body'>
                                <h5 class='card-title'>{$foodName}</h5>
                                <p class='card-text'>{$foodDesc}</p>
                                <p class='card-text'>{$foodPrice} Ft - Popularity: {$foodPopularity}</p>
                            </div>
                        </div>
                        </div>
                        ";
                    }   
                }
            }
            echo $html;
        } else {
            echo "allfood";
        }
    }

    public function showPopularFoods()
    {
        $foodDetails = $this->getPopularFoods(5);

        $html = "
        <div id='indicator' class='carousel slide' data-ride='carousel'>
        <div class='carousel-inner'>
        ";
        foreach($foodDetails as $i => $food) {
            $foodName = $food['name'];
            $foodPopularity = $food['popularity'];
            $foodPrice = $food['price'];
            $foodType = $food['type'];

            if ($i == 4) {
                $html .= "
                <div class='carousel-item active'>
                    <img class='d-block w-100' src='img/{$foodType}0.jpg' alt='First slide'>
                    <div class='carousel-caption d-none d-md-block bg-light text-dark'>
                        <h5>{$foodName}</h5>
                        <p>{$foodPrice} Ft</p>
                        <p>Popularity: {$foodPopularity}</p>
                        <a href='foods.php' type='button' class='btn btn-primary'>To Foods</a>
                    </div>
                </div>
                ";
            } else {
                $html .= "
                <div class='carousel-item'>
                    <img class='d-block w-100' src='img/{$foodType}{$i}.jpg' alt='First slide'>
                    <div class='carousel-caption d-none d-md-block bg-light text-dark'>
                        <h5>{$foodName}</h5>
                        <p>{$foodPrice} Ft</p>
                        <p>Popularity: {$foodPopularity}</p>
                        <a href='foods.php' type='button' class='btn btn-primary'>To Foods</a>
                    </div>
                </div>
                ";
            }
        }
        
        $html .= "
        </div>
            <a class='carousel-control-prev' href='#indicator' role='button' data-slide='prev'>
                <span class='carousel-control-prev-icon' aria-hidden='true'></span>
                <span class='sr-only'>Previous</span>
            </a>
            <a class='carousel-control-next' href='#indicator' role='button' data-slide='next'>
                <span class='carousel-control-next-icon' aria-hidden='true'></span>
                <span class='sr-only'>Next</span>
            </a>
        </div>
        ";
        echo $html;
    }
}








/* <div id='indicator' class='carousel slide' data-ride='carousel'>
    <div class='carousel-inner'>
        <div class='carousel-item'>
            <img class='d-block w-100' src='img/hamburger0.jpg' alt='First slide'>
            <div class='carousel-caption d-none d-md-block bg-light text-dark'>
                <h5>Name</h5>
                <p>22</p>
                <p>Popularity: 2</p>
                <a href='foods.php' type='button' class='btn btn-primary'>To Foods</a>
            </div>
        </div>

        <div class='carousel-item'>
            <img class='d-block w-100' src='img/pasta2.jpg' alt='First slide'>
            <div class='carousel-caption d-none d-md-block bg-light text-dark'>
                <h5>ASD</h5>
                <p>213</p>
                <p>Popularity: 213</p>
                <a href='foods.php' type='button' class='btn btn-primary'>To Foods</a>
            </div>
        </div>
    </div>
    <a class='carousel-control-prev' href='#indicator' role='button' data-slide='prev'>
        <span class='carousel-control-prev-icon' aria-hidden='true'></span>
        <span class='sr-only'>Previous</span>
    </a>
    <a class='carousel-control-next' href='#indicator' role='button' data-slide='next'>
        <span class='carousel-control-next-icon' aria-hidden='true'></span>
        <span class='sr-only'>Next</span>
    </a>
</div> */
