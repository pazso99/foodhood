<?php

require_once './../vendor/autoload.php';

use App\Controllers\UserController;

$TITLE = "New Password";

include './../app/Views/inc/header.php';
?>

<div class="wrapper">
    <h1 class="title"><?= $TITLE ?></h1>

<?php
if (isset($_SESSION['username']) && $_SESSION['secured'] == 0) {

    if (isset($_POST['submitPassword'])) {
    
        // create user controller
        $userController = new UserController();
        $errors = $userController->addNewPassword($_POST, $_SESSION['username']);
    
        if (!empty($errors)) {
            foreach($errors as $error => $errorText){
                echo "<p class='error'>{$error} ERROR: {$errorText}</p>";
            }
        }
    }
} else {
    header("Location: profile.php");
}
?>
<h2>Sorry, but we have been hacked and the passwords has been leaked, we want you to change password!</h2>
<form class="form" action="newpassword.php" method="POST">

    <label for="password">Password</label>
    <input type="password" name="password" id="password">

    <label for="passwordRepeat">Repeat password</label>
    <input type="password" name="passwordRepeat" id="passwordRepeat">

    <button class="submit-button" name="submitPassword" type="submit">Update</button>
</form>

<?php
include './../app/Views/inc/footer.php';
?>
