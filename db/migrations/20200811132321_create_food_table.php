<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateFoodTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $products = $this->table('foods');
        $products->addColumn('name', 'string', ['limit' => 75])
                ->addColumn('type', 'string', ['limit' => 75])
                ->addColumn('description', 'string')
                ->addColumn('price', 'integer')
                ->addColumn('popularity', 'integer')
                ->addColumn('created_at', 'datetime')
                ->addIndex(['name'], ['unique' => true])
                ->create();
    }
}
