<?php

require_once './../vendor/autoload.php';

use App\Controllers\UserController;

$TITLE = "Signup";

include './../app/Views/inc/header.php';
?>

<div class="wrapper">
    <h1 class="title"><?= $TITLE ?></h1>

<?php
if (isset($_POST['signupButton'])) {
    
    // create user controller
    $userController = new UserController();

    $errors = $userController->signupUser($_POST);

    if (!empty($errors)) {
        foreach($errors as $error => $errorText){
            echo "<p class='error'>{$error} ERROR: {$errorText}</p>";
        }
    }
}
?>
<form class="form" action="signup.php" method="POST">
    <label for="username">Username</label>
    <input type="text" name="username" id="username" value="<?= isset($_POST['username']) ? htmlspecialchars($_POST['username']) : '' ?>">
    
    <label for="password">Password</label>
    <input type="password" name="password" id="password">

    <label for="passwordRepeat">Repeat password</label>
    <input type="password" name="passwordRepeat" id="passwordRepeat">

    <label for="email">Email</label>
    <input type="text" name="email" id="email" value="<?= isset($_POST['email']) ? htmlspecialchars($_POST['email']) : '' ?>">
    
    <label for="firstName">First name</label>
    <input type="text" name="firstName" id="firstName" value="<?= isset($_POST['firstName']) ? htmlspecialchars($_POST['firstName']) : '' ?>">

    <label for="lastName">Last name</label>
    <input type="text" name="lastName" id="lastName" value="<?= isset($_POST['lastName']) ? htmlspecialchars($_POST['lastName']) : '' ?>">

    <label for="address">Address</label>
    <input type="text" name="address" id="address" value="<?= isset($_POST['address']) ? htmlspecialchars($_POST['address']) : '' ?>">

    <button class="submit-button" name="signupButton" type="submit">Sign up</button>
</form>

<?php
include './../app/Views/inc/footer.php';
?>
