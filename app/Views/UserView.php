<?php

namespace App\Views;

use App\Controllers\UserController;

# Get data
class UserView extends UserController
{
    public function showUser($name)
    {
        $userDetails = $this->getUser($name);

        $username = $userDetails['username'];
        $email = $userDetails['email'];
        $firstName = $userDetails['first_name'];
        $lastName = $userDetails['last_name'];
        $address = $userDetails['address'];
        $secured = $userDetails['first_login'];
        $created = $userDetails['created_at'];

        echo "
        <div class='user-box'>
            <p><span class='user-detail'>Username: </span>{$username}</p>
            <p><span class='user-detail'>Email: </span>{$email}</p>
            <p><span class='user-detail'>First name: </span>{$firstName}</p>
            <p><span class='user-detail'>Last name: </span>{$lastName}</p>
            <p><span class='user-detail'>Address: </span>{$address}</p>
            <p><span class='user-detail'>Account created at: </span>{$created}</p>
            <p><span class='user-detail'>Account Secured: </span>{$secured}</p>
        </div>
        ";
    }
}
