<?php

require_once './../vendor/autoload.php';

use App\Views\UserView;

$TITLE = "Profile";

include './../app/Views/inc/header.php';
?>

<div class="wrapper">
    <h1 class="title"><?= $TITLE ?></h1>

<?php
if (isset($_SESSION['username'])) {
    if ($_SESSION['secured'] == 1) {
        $userView = new UserView();
        $userView->showUser($_SESSION['username']);
    } else {
        header("Location: newpassword.php");
    }
} else {
    header("Location: ./");
}
?>

<?php
include './../app/Views/inc/footer.php';
?>
