<?php

namespace App\Controllers\Validators;

class FoodValidator
{
    private $formData;
    private $errors = [];
    
    public function __construct($formData)
    {
        $this->formData = $formData;
    }

    // Add error to associative array
    private function addError($key, $val)
    {
        $this->errors[$key] = $val;
    }

    // User signup start
    public function validateFoodInsert()
    {
        $this->validateStringData($this->formData['firstName']);
        $this->validateStringData($this->formData['lastName']);
        $this->validateStringData($this->formData['address']);
        return $this->errors;
    }
    

    // String data validation
    private function validateStringData($stringData)
    {    
        $stringData = trim($stringData);

        if (empty($stringData) && !array_key_exists('fields', $this->errors)) {
            $this->addError('fields', 'all fields must be filled');
        }
    }
    
    // Numebr data validation
    /*private function validateNumberData() {
        
         $username = trim($this->formData['username']);

        if(empty($username)){
            $this->addError('username', 'username cannot be empty');
        } 
    }*/
}  