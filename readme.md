# FoodHood

## Features

- Regisztráció
- Bejelentkezés, utánna új jelszó változtatás, mert feltörték az oldalt :(
- Kapcsolati fül
- Étlap fül


## Installation
- ### FoodHood mappa készítése, majd repo clone-ozása
```bash
git clone https://gitlab.com/pazso99/foodhood.git
```

- ### npm install
```bash
npm install
```

- ### composer install
```bash
composer install
```

- ### Adatbázis adatok
  - foodhood_db adatbázis készítése pgAdmin4-ben
  - .env.example megfelelő kitöltése, majd átnevezés .env-re
```
DB_ADAPTER="pgsql"
DB_HOST="localhost"
DB_NAME="foodhood_db"
DB_USER="username"
DB_PASSWORD="password"
DB_PORT="3306"
```
- ### Adatbázis készítése && seedelés
```bash
npm run db
```