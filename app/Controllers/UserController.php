<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Controllers\Validators\UserValidator;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

# Updates, Inserts database with the UsersModel
class UserController extends UserModel
{
    public function signupUser($formData) 
    {
        // start validation - get errors back
        $validation = new UserValidator($formData);
        $errors = $validation->validateUserSignup();

        if (empty($errors)) {
            if (!$this->userExist($formData['username'])) {
                $this->addUser($formData);
            } else {
                $errors['username'] = 'This User Already Exists';
            }
        }
        return $errors;
    }

    public function loginUser($formData) 
    {
        // start validation - get errors back
        $validation = new UserValidator($formData);
        $errors = $validation->validateUserLogin();

        if (empty($errors)) {
            if ($this->userExist($formData['username'])) {
                $userDetails = $this->getUser($formData['username']);

                $pwdCheck = password_verify($formData['password'], $userDetails['password']);
                
                if ($pwdCheck) {
                    session_start();
                    
                    $_SESSION['username'] = $userDetails['username'];
                    $_SESSION['email'] = $userDetails['email'];
                    $_SESSION['firstName'] = $userDetails['first_name'];
                    $_SESSION['lastName'] = $userDetails['last_name'];
                    $_SESSION['address'] = $userDetails['address'];
                    $_SESSION['secured'] = $userDetails['first_login'];
                    $_SESSION['created'] = $userDetails['created_at'];

                    header("Location: profile.php");
                    exit();
                } else {
                    $errors['username'] = 'Wrong password';
                }
            } else {
                $errors['username'] = 'This User Not Exists';
            }
        }
        return $errors;
    }

    public function sendEmail($formData)
    {
        // start validation - get errors back
        $validation = new UserValidator($formData);
        $errors = $validation->validateContactForm($formData);

        if (empty($errors)) {
            $name = $formData['name'];
            $email = $formData['email'];
            $message = $formData['message'];

            $mail = new PHPMailer(true);
            
            try {
                //Server settings
                $mail->isSMTP();
                $mail->Host       = 'smtp.gmail.com';
                $mail->SMTPAuth   = true;
                $mail->Username   = 'email@email.em';
                $mail->Password   = '123';
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; 
                $mail->Port       = 587;

                $mail->setFrom($email, $name);
                $mail->addAddress('email@email.em'); 
                $mail->isHTML(true);
                $mail->Subject = "Email from {$name}";
                $mail->Body    = $message;

                $mail->send();
            } catch (Exception $e) {
                $errors['mail'] = "Message could not be sent.";
            }
        }

        return $errors;
    }

    public function addNewPassword($formData, $username) 
    {
        // start validation - get errors back
        $validation = new UserValidator($formData);
        $errors = $validation->validateNewPassword();

        if (empty($errors)) {
            $checkPass = $this->getUser($username);
            if (!password_verify($formData['password'], $checkPass['password'])) {

                $this->updatePassword($username, $formData['password']);
                $userDetails = $this->getUser($username);

                session_start();
                
                $_SESSION['username'] = $userDetails['username'];
                $_SESSION['email'] = $userDetails['email'];
                $_SESSION['firstName'] = $userDetails['first_name'];
                $_SESSION['lastName'] = $userDetails['last_name'];
                $_SESSION['address'] = $userDetails['address'];
                $_SESSION['secured'] = $userDetails['first_login'];
                $_SESSION['created'] = $userDetails['created_at'];


                header("Location: profile.php");
                exit();
            } else {
                $errors['password'] = 'Cannot be same as old password';
            }
        }
        return $errors;
    }
}
