<?php

require_once './../vendor/autoload.php';

use App\Views\FoodView;

if (isset($_POST['send'])) {
    $i = $_POST['send'];
    $foodView = new FoodView();
    $foodView->showFoods($i);
    exit;
}

$TITLE = "Food list";
include './../app/Views/inc/header.php';
?>

<div class="wrapper">
    <h1 class="title"><?= $TITLE ?></h1>

<style>
    .card .card-image-top {
        max-width: 308px !important;
    }
</style>
<div class="food-list">
<?php
    $foodView = new FoodView();
    $foodView->showFoods(10);
?>
</div>
<button id="loadMore" type="button" class="btn btn-primary btn-lg btn-block">Load More</button>
<script>
i = 20;
document.getElementById('loadMore').addEventListener('click', () => {
	
	const xhr = new XMLHttpRequest();
	xhr.open('POST', 'foods.php', true);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    
	xhr.onload = function() {
        if (this.responseText == "allfood") {;
            alert("All food listed!");
        } else {
            document.querySelector('.food-list').innerHTML = this.responseText;
            i+=10;
        }
    }
	xhr.send(`send=${i}`);
});
</script>

<?php
include './../app/Views/inc/footer.php';
?>
