<?php

require_once './../vendor/autoload.php';

use App\Controllers\UserController;

$TITLE = "Login";

include './../app/Views/inc/header.php';
?>

<div class="wrapper">
    <h1 class="title"><?= $TITLE ?></h1>

<?php
if (isset($_GET['signup'])) {
    echo "<p class='success'>Successful signup!</p>";
} elseif (isset($_POST['loginButton'])) {
    // create user controller
    $userController = new UserController();

    $errors = $userController->loginUser($_POST);

    if (!empty($errors)) {
        foreach($errors as $error => $errorText){
            echo "<p class='error'>{$error} ERROR: {$errorText}</p>";
        }
    }
}
?>
<form class="form" action="login.php" method="POST" style="margin-bottom: 140px">
    <label for="username">Username</label>
    <input type="text" name="username" id="username" value="<?= isset($_POST['username']) ? htmlspecialchars($_POST['username']) : '' ?>">
    
    <label for="password">Password</label>
    <input type="password" name="password" id="password">

    <button class="submit-button" name="loginButton" type="submit">Login</button>
</form>

<?php
include './../app/Views/inc/footer.php';
?>