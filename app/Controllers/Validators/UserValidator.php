<?php

namespace App\Controllers\Validators;

class UserValidator
{
    private $formData;
    private $errors = [];
    
    public function __construct($formData)
    {
        $this->formData = $formData;
    }

    // Add error to associative array
    private function addError($key, $val)
    {
        $this->errors[$key] = $val;
    }

    // User signup start
    public function validateUserSignup()
    {
        $this->validateStringData($this->formData['firstName']);
        $this->validateStringData($this->formData['lastName']);
        $this->validateStringData($this->formData['address']);
        $this->validateUsername();
        $this->validateEmail();
        $this->validatePassword();
        return $this->errors;
    }

    // User login start
    public function validateUserLogin() 
    {
        $this->validateStringData($this->formData['username']);
        $this->validateStringData($this->formData['password']);
        return $this->errors;
    }

    // Form data input validation
    public function validateContactForm()
    {    
        $this->validateStringData($this->formData['name']);
        $this->validateStringData($this->formData['message']);
        $this->validateEmail();
        return $this->errors;
    }

    // new password validation
    public function validateNewPassword()
    {    
        $this->validatePassword();
        return $this->errors;
    }
    
    // Username validation
    private function validateUsername()
    {
        $username = trim($this->formData['username']);

        if (empty($username)) {
            $this->addError('username', 'username cannot be empty');
        } elseif (!preg_match("/^[\d\w@#$&_]{3,}$/", $username)) {
            $this->addError('username', 'wrong username');
        }
    }

    // Password validation
    private function validatePassword()
    {
        $password = trim($this->formData['password']);
        $repeatPassword = trim($this->formData['passwordRepeat']);

        if (empty($password) || empty($repeatPassword)) {
            $this->addError('password', 'password cannot be empty');
        } elseif (!preg_match("/^[\d\w@#$&_]{8,}$/", $password)) {
            $this->addError('password', 'password is not correct, must be 8 characters');
        } elseif ($password !== $repeatPassword) {
            $this->addError('password', 'passwords not match');
        }
    }

    // Email validation
    private function validateEmail()
    {
        $email = trim($this->formData['email']);

        if(empty($email)) {
            $this->addError('email', 'email cannot be empty');
        } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->addError('email', 'email is not correct');
        }
    }

    // String data validation
    private function validateStringData($stringData)
    {    
        $stringData = trim($stringData);

        if (empty($stringData) && !array_key_exists('fields', $this->errors)) {
            $this->addError('fields', 'all fields must be filled');
        }
    }
    
    // Numebr data validation
    /*private function validateNumberData() {
        
         $username = trim($this->formData['username']);

        if(empty($username)){
            $this->addError('username', 'username cannot be empty');
        } 
    }*/
}  