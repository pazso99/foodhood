<?php

namespace App\Models;

use App\Models\Database\Database;

# model - interact with DATABASE
//SELECT * FROM foods ORDER BY popularity DESC LIMIT 5
class UserModel extends Database
{
    protected function userExist($username)
    {
        $sql = "SELECT 
                    * 
                FROM 
                    users 
                WHERE 
                    username = ?";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$username]);
        $count = $stmt->rowCount();

        return $count > 0;
    }

    protected function getUserCount()
    {
        $sql = "SELECT 
                    * 
                FROM 
                    users";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $count = $stmt->rowCount();

        return $count;
    }

    protected function getUser($username)
    {
        $sql = "SELECT 
                    * 
                FROM 
                    users 
                WHERE 
                    username = ?";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$username]);
        $result = $stmt->fetch();

        return $result;
    }

    protected function addUser($formData)
    {
        $username = $formData['username'];
        $hashedPwd = password_hash($formData['password'], PASSWORD_DEFAULT);
        $email = $formData['email'];
        $firstName = $formData['firstName'];
        $lastName = $formData['lastName'];
        $address = $formData['address'];
        $createdAt = date("Y-m-d H:i:s");

        $sql = "INSERT INTO 
                    users (username, password, email, first_name, last_name, address, first_login, created_at) 
                VALUES 
                    (?, ?, ?, ?, ?, ?, ?, ?)";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$username, $hashedPwd, $email, $firstName, $lastName, $address, 0, $createdAt]);

        header('Location: login.php?signup=succes');
    }

    protected function updatePassword($username, $newPassword)
    {
        $hashedPwd = password_hash($newPassword, PASSWORD_DEFAULT);
        $sql = "UPDATE 
                    users 
                SET 
                    first_login = 1,
                    password = ? 
                WHERE 
                    username = ?";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$hashedPwd, $username]);
    }
}
