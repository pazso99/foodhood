<?php

require_once './../vendor/autoload.php';

use App\Controllers\UserController;
    
$TITLE = "Contact";
include './../app/Views/inc/header.php';
?>

<div class="wrapper">
    <h1 class="title"><?= $TITLE ?></h1>

<?php
if (isset($_POST['sendButton'])) {
    // create user controller
    $userController = new UserController();

    $errors = $userController->sendEmail($_POST);

    if (!empty($errors)) {
        foreach ($errors as $error => $errorText){
            echo "<p class='error'>{$error} ERROR: {$errorText}</p>";
        }
    } else {
        echo "<p class='success'>Email successfully sent!</p>";
    }
}
?>

<form class="form" action="contact.php" method="POST">
    <label for="name">Name</label>
    <input type="text" name="name" id="name" value="<?= isset($_POST['name']) ? htmlspecialchars($_POST['name']) : '' ?>">
    
    <label for="email">Email</label>
    <input type="text" name="email" id="email" value="<?= isset($_POST['email']) ? htmlspecialchars($_POST['email']) : '' ?>">

    <label for="message">Message</label>
	<textarea id="message" name="message" rows="5"><?= isset($_POST['message']) ? htmlspecialchars($_POST['message']) : '' ?></textarea>
			
    <button class="submit-button" name="sendButton" type="submit">Send email</button>
</form>

<?php
include './../app/Views/inc/footer.php';
?>
