<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateOrderDetailsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $orderDetails = $this->table('order_details');
        $orderDetails->addColumn('order_id', 'integer')
                ->addColumn('food_id', 'integer')
                ->addColumn('quantity', 'integer')
                ->addForeignKey('order_id', 'orders', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                ->addForeignKey('food_id', 'foods', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                ->create();
    }
}
