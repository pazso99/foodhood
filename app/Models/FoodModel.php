<?php

namespace App\Models;

use App\Models\Database\Database;

class FoodModel extends Database
{
    // Check if food exists
    protected function foodExist($name)
    {
        $sql = "SELECT 
                    * 
                FROM 
                    foods 
                WHERE 
                    name = ?";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$name]);
        $count = $stmt->rowCount();

        return $count > 0;
    }

    protected function getFoodCount()
    {
        $sql = "SELECT 
                    * 
                FROM 
                    foods";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $count = $stmt->rowCount();

        return $count;
    }

    // Get one food details
    protected function getFood($name)
    {
        $sql = "SELECT 
                    * 
                FROM 
                    foods 
                WHERE 
                    name = ?";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$name]);
        $result = $stmt->fetch();

        return $result;
    }
    
    // Get $amount food details
    protected function getFoods($amount)
    {
        $sql = "SELECT 
                    * 
                FROM 
                    foods
                LIMIT ?";
    
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$amount]);
        $result = $stmt->fetchAll();
    
        return $result;
    }

    // Get all food details
    protected function getAllFood()
    {
        $sql = "SELECT 
                    * 
                FROM 
                    foods";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    // get the most popular foods
    protected function getPopularFoods($amount)
    {
        $sql = "SELECT 
                    * 
                FROM 
                    foods 
                ORDER BY 
                    popularity
                DESC
                LIMIT ?";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$amount]);
        $result = $stmt->fetchAll();

        return $result;
    }

    // add food to db
    protected function addFoodToDB($formData)
    {
        $foodName = $formData['name'];
        $foodType = $formData['type'];
        $foodDescription = $formData['description'];
        $foodPrice = $formData['price'];
        $foodPopularity = 0;
        $createdAt = date("Y-m-d H:i:s");

        $sql = "INSERT INTO 
                    foods (name, type, description, price, popularity, created_at) 
                VALUES 
                    (?, ?, ?, ?, ?, ?)";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$foodName, $foodType, $foodDescription, $foodPrice, $foodPopularity, $createdAt]);

        header('Location: ');
    }
}
