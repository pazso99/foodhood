<?php

require_once './../vendor/autoload.php';

use App\Views\FoodView;

$TITLE = "Home";

include './../app/Views/inc/header.php';
?>

<header class="welcome">
    <img class="welcome-img" src="img/foodhood.png" alt="FoodHood" height="800">
    <div class="welcome-title"> 
        <h1>FoodHood</h1>
    </div>
    <div class="welcome-sub mb-4">
        <h2>FoodHood - Order. Best. Food.</h2>
    </div>
</header>
<hr class="mb-4">
<h3 class="text-center pb-2">Most popular foods</h3>
<div class="carousel-wrapper">
<?php
   $foodView = new FoodView();
   $foodView->showPopularFoods();
?> 
</div>

<hr>
<div class="wrapper">
<div class="about">
    <div class="about-img">
        <img src="img/about1.png" class="" alt="About" width="500">
    </div>

    <div class="about-text">
        <h1>About FoodHood</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate autem eum rerum accusantium ducimus, praesentium officiis earum ex. Sunt, porro. Repellendus maiores et tenetur eveniet! Veniam culpa debitis voluptate reprehenderit expedita consequatur, odio inventore distinctio. Molestias, saepe sit. Beatae fugiat provident dolor nemo.</p>
    </div>
</div>
<script>
$('.carousel').carousel({
    interval: 2000
})
</script>
<?php
include './../app/Views/inc/footer.php';
?>
