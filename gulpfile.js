const gulp = require('gulp');
const sass = require('gulp-sass');

// bootstrap and scss -> css
function css() {
    return gulp.src([
            './public/scss/*.scss',
            './node_modules/bootstrap/scss/bootstrap.scss'
        ])
        .pipe(sass())
        .pipe(gulp.dest('./public/css'));
}

// bootstrap and jquerry and popper -> js
function js() {
    return gulp.src([
            './node_modules/bootstrap/dist/js/bootstrap.min.js',
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/popper.js/dist/umd/popper.min.js'
        ])
        .pipe(gulp.dest('./public/js'));
}
/* 
npm init
npm install gulp gulp-sass --save-dev
npm install bootstrap jquery popper.js --save
npm install -g gulp-cli  
 */
exports.default = gulp.series(css, js);