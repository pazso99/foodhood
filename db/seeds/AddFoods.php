<?php


use Phinx\Seed\AbstractSeed;

class AddFoods extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    //php vendor/bin/phinx seed:run
    public function run()
    {
        $faker = Faker\Factory::create();
        $faker->addProvider(new \FakerRestaurant\Provider\en_US\Restaurant($faker));

        $data = [];
        for ($i = 0; $i < 30; $i++) {
            $color = join(" ", preg_split('/(?=[A-Z])/', $faker->colorName, -1, PREG_SPLIT_NO_EMPTY));
            $name = "{$color} {$faker->foodName()}";
            $price = round($faker->numberBetween(500, 5000), -1);

            if (strpos(strtolower($name), 'burger')) {
                $type = "hamburger";
            } elseif (strpos(strtolower($name), 'sandwich')) {
                $type = "sandwich";
            } elseif (strpos(strtolower($name), 'pasta')) {
                $type = "pasta";
            } elseif (strpos(strtolower($name), 'pizza')) {
                $type = "pizza";
            } elseif (strpos(strtolower($name), 'dog')) {
                $type = "hotdog";
            } elseif (strpos(strtolower($name), 'cheese')) {
                $type = "cheese";
            }

            if (!in_array($name, $data)) {
                $data[] = [
                    'name' => $name,
                    'type' => $type,
                    'description' => $faker->text(rand(100,150)),
                    'price' => $price,
                    'popularity' => $faker->numberBetween(1, 50),
                    'created_at' => date('Y-m-d H:i:s'),
                ];
            } else {
                $i--;
            }
        }
            
        $this->table('foods')->insert($data)->save();
    }
}
