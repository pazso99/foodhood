<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FoodHood - <?= $TITLE ?></title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script src="js/jquery.min.js" defer></script>
    <script src="js/bootstrap.min.js" defer></script>
    <script src="js/responsive.js" defer></script>
</head>
<body>
   <nav>
        <ul class="menu">
            <li class="logo"><a href="./">FoodHood</a></li>
            <li class="item"><a href="foods.php">Foods</a></li>
            <li class="item"><a href="contact.php">Contact</a></li>
            <?=
            isset($_SESSION['username']) ? 
            '
            <li class="item button"><a href="profile.php">' . ucfirst(strtolower($_SESSION['username'])) . '</a></li>
            <li class="item button secondary"><a href="logout.inc.php">Logout</a></li>
            '
            :
            '
            <li class="item button secondary"><a href="login.php">Login</a></li>
            <li class="item button"><a href="signup.php">Signup</a></li>
            ';
            ?>
            <li class="toggle"><span class="bars"></span></li>
        </ul>
   </nav>
